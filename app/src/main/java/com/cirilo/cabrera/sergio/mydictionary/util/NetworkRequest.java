package com.cirilo.cabrera.sergio.mydictionary.util;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class NetworkRequest extends AsyncTask<String, Integer, Map<Integer, Object>> {

    public OnRequestFinishListener onRequestFinishListener;
    private final int RESPONSE_CODE = 0;
    private final int RESPONSE_DATA = 1;

    public NetworkRequest(OnRequestFinishListener onRequestFinishListener) {
        this.onRequestFinishListener = onRequestFinishListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        onRequestFinishListener.onPrepare();
    }

    @Override
    protected Map<Integer, Object> doInBackground(String... locations) {
        publishProgress(0);
        Map<Integer, Object> map = new HashMap<>();
        try {
            URL url = new URL("http://api.weatherstack.com/current?access_key=f0d6547a72812392d6f28c1046889e76&query=" + locations[0]);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setRequestMethod("GET");
            conn.setConnectTimeout(10 * 1000);
            conn.connect();

            int code = conn.getResponseCode();
            if (code == HttpsURLConnection.HTTP_OK) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder builder = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null ) {
                    builder.append(line);
                }
                reader.close();

                map.put(RESPONSE_CODE, 200);
                map.put(RESPONSE_DATA, builder.toString());
            } else {
                map.put(RESPONSE_CODE, code);
                map.put(RESPONSE_DATA, conn.getResponseMessage());
            }
        } catch(IOException ex) {
            map.put(RESPONSE_CODE, 0);
            map.put(RESPONSE_DATA, "Exception: " + ex.getMessage());
        }

        //Solo para hacer visible el progreso
        for(int i = 0; i < 100; i++) {
            try {
                Thread.sleep(50);
                publishProgress(i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return map;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        onRequestFinishListener.onProgress(values[0]);
    }

    @Override
    protected void onPostExecute(Map<Integer, Object> response) {
        super.onPostExecute(response);

        if(((Integer) response.get(RESPONSE_CODE)) == 200) {
            String data = String.valueOf(response.get(RESPONSE_DATA));
            try{
                JSONObject json = new JSONObject(data);
                onRequestFinishListener.onResponseSuccess(json);
            } catch (JSONException ex) {
                onRequestFinishListener.onResponseError(1, ex.getMessage());
            }
        } else {
            onRequestFinishListener.onResponseError((int) response.get(RESPONSE_CODE), String.valueOf(response.get(RESPONSE_DATA)));
        }

        onRequestFinishListener.onResponseFinish();
    }



    public interface OnRequestFinishListener {

        void onResponseSuccess(JSONObject data);

        void onResponseError(int code, String message);

        void onResponseFinish();

        void onPrepare();

        void onProgress(int percentage);
    }
}

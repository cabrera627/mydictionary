package com.cirilo.cabrera.sergio.mydictionary.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cirilo.cabrera.sergio.mydictionary.R;
import com.cirilo.cabrera.sergio.mydictionary.model.Dictionary;

import java.util.List;

public class DictionaryAdapter extends RecyclerView.Adapter<DictionaryHolder> {
    private List<Dictionary> dictionaryList;
    private DictionaryHolder.OnItemClickListener listener;

    public DictionaryAdapter(List<Dictionary> dictionaryList, DictionaryHolder.OnItemClickListener listener) {
        this.dictionaryList = dictionaryList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public DictionaryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
        DictionaryHolder holder = new DictionaryHolder(view);
        holder.listener = listener;
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull DictionaryHolder holder, int position) {
        holder.title.setText(dictionaryList.get(position).getTitle());
        holder.item = dictionaryList.get(position);
        holder.position = position;
    }

    @Override
    public int getItemCount() {
        return dictionaryList.size();
    }

    public void remove(int position) {
        dictionaryList.remove(position);
    }

    public Dictionary getItem(int position) {
        return dictionaryList.get(position);
    }

    public boolean addItem(Dictionary dictionary) {
        return dictionaryList.add(dictionary);
    }
}

package com.cirilo.cabrera.sergio.mydictionary;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;

import com.cirilo.cabrera.sergio.mydictionary.model.Dictionary;
import com.cirilo.cabrera.sergio.mydictionary.sqldata.DBManager;

public class FormDictionaryFragment extends Fragment {

    private EditText mTitle;
    private EditText mDescription;
    private Button mAddButton;
    private Listeners listener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_form_dictionary, container, false);
        initComponents(root);
        return root;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initListeners();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (Listeners) context;
        } catch (ClassCastException ex) {
            ex.printStackTrace();
        }
    }

    private void initComponents(View root) {
        mTitle = root.findViewById(R.id.edtFormTitle);
        mDescription = root.findViewById(R.id.edtFormDescription);
        mAddButton = root.findViewById(R.id.btnFormAdd);
    }

    private void initListeners() {
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validate()) {
                    Dictionary dictionary = new Dictionary();
                    dictionary.setTitle(mTitle.getText().toString());
                    dictionary.setContent(mDescription.getText().toString());
                    DBManager db = new DBManager(getContext());
                    db.insert(dictionary);

                    dictionary = db.getLastDictionary();
                    listener.dataHasChanged(dictionary);
                    listener.performBack();
                }

            }
        });
    }

    private boolean validate() {
        if(TextUtils.isEmpty(mTitle.getText()) || TextUtils.isEmpty(mDescription.getText())) {
            listener.showAlert(R.string.form_empty_fields);
            return false;
        }
        return true;
    }


    public interface Listeners {
        void showAlert(@StringRes int message);
        void dataHasChanged(Dictionary dictionary);
        void performBack();
    }
}
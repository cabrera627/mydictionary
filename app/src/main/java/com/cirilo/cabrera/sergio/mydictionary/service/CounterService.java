package com.cirilo.cabrera.sergio.mydictionary.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;

import androidx.annotation.Nullable;

import static com.cirilo.cabrera.sergio.mydictionary.constants.LoginConstants.PREF_TIME_CONNECTED;

public class CounterService extends Service {
    private final static String NAME = "MyDictionary";
    private boolean STOP_TIMER = false;
    private long secondsElapsed = 0;
    private SharedPreferences preferences;

    @Override
    public void onCreate() {
        preferences = getSharedPreferences(NAME, Context.MODE_PRIVATE);
        secondsElapsed = preferences.getLong(PREF_TIME_CONNECTED, 0L);
        final Handler h = new Handler();
        h.post(new Runnable() {
            @Override
            public void run() {
                secondsElapsed++;
                if(!STOP_TIMER)
                    h.postDelayed(this, 1000);
            }
        });

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        STOP_TIMER = true;
        preferences.edit().putLong(PREF_TIME_CONNECTED, secondsElapsed).apply();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


}

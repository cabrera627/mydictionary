package com.cirilo.cabrera.sergio.mydictionary.model;

import java.io.Serializable;

public class Dictionary implements Serializable {
    private int id;
    private String title;
    private String content;

    public Dictionary() {
    }

    public Dictionary(int id, String title, String content) {
        this.id = id;
        this.title = title;
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

package com.cirilo.cabrera.sergio.mydictionary;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;

import com.cirilo.cabrera.sergio.mydictionary.util.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static com.cirilo.cabrera.sergio.mydictionary.constants.LoginConstants.PREF_IS_LOGIN;
import static com.cirilo.cabrera.sergio.mydictionary.constants.LoginConstants.PREF_LAST_CONNECTION;
import static com.cirilo.cabrera.sergio.mydictionary.constants.LoginConstants.PREF_USER_ID;
import static com.cirilo.cabrera.sergio.mydictionary.constants.LoginConstants.PREF_USER_LOCATION;
import static com.cirilo.cabrera.sergio.mydictionary.constants.LoginConstants.PREF_USER_NAME;

public class LoginActivity extends BaseActivity {

    private EditText mID;
    private EditText mPassword;
    private Button mLoginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if(isLogged()) goToMain();
        initComponents();
        initListeners();

    }

    private void initComponents() {
        mID = findViewById(R.id.edtID);
        mPassword = findViewById(R.id.edtPassword);
        mLoginButton = findViewById(R.id.btnLogin);
    }

    private void initListeners() {
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogin();
            }
        });
    }

    private void attemptLogin() {
        if(TextUtils.isEmpty(mID.getText()) && TextUtils.isEmpty(mPassword.getText())) {
            showAlert(this, R.string.login_empty_fields);
            return;
        }

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("users").child(mID.getText().toString());

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(snapshot.exists() && snapshot.child("password").exists() && snapshot.child("password").getValue().toString().equals(mPassword.getText().toString())) {
                    writePreferences(PREF_IS_LOGIN, true);
                    writePreferences(PREF_USER_NAME, snapshot.child("name").getValue());
                    writePreferences(PREF_USER_LOCATION, snapshot.child("location").getValue());
                    writePreferences(PREF_USER_ID, mID.getText().toString());
                    writePreferences(PREF_LAST_CONNECTION, System.currentTimeMillis());
                    goToMain();
                } else {
                    showAlert(LoginActivity.this, R.string.login_incorrect_user_password);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                showAlert(LoginActivity.this, R.string.login_error);
            }
        });
    }

    private void goToMain() {
        Utils.USER_ID = (String)getFromPreferences(PREF_USER_ID, "");
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}
package com.cirilo.cabrera.sergio.mydictionary.sqldata;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.cirilo.cabrera.sergio.mydictionary.model.Dictionary;
import com.cirilo.cabrera.sergio.mydictionary.util.Utils;

import java.util.ArrayList;
import java.util.List;

import static android.provider.BaseColumns._ID;
import static com.cirilo.cabrera.sergio.mydictionary.sqldata.SQLiteContract.DictionaryEntries.COLUMNS;
import static com.cirilo.cabrera.sergio.mydictionary.sqldata.SQLiteContract.DictionaryEntries.CONTENT;
import static com.cirilo.cabrera.sergio.mydictionary.sqldata.SQLiteContract.DictionaryEntries.TABLE_NAME;
import static com.cirilo.cabrera.sergio.mydictionary.sqldata.SQLiteContract.DictionaryEntries.TITLE;
import static com.cirilo.cabrera.sergio.mydictionary.sqldata.SQLiteContract.DictionaryEntries.USER_ID;

public class DBManager extends SQLite {

    public DBManager(Context context) {
        super(context);
    }

    public List<Dictionary> getDictionaryList() {
        List<Dictionary> items = null;
        SQLiteDatabase db = getReadableDatabase();
        try {
            Cursor cursor = db.query(TABLE_NAME, COLUMNS, USER_ID + " = ?", new String[]{Utils.USER_ID}, null, null, null);
            items = new ArrayList<>();
            while (cursor.moveToNext()) {
                Dictionary item = new Dictionary();
                item.setId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
                item.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(TITLE)));
                item.setContent(cursor.getString(cursor.getColumnIndexOrThrow(CONTENT)));
                items.add(item);
            }
            cursor.close();
        } catch(Exception e) {

        } finally {
            db.close();
        }
        return items;
    }

    public Dictionary getLastDictionary() {
        Dictionary dictionary = null;
        SQLiteDatabase db = getReadableDatabase();
        try {
            Cursor cursor = db.query(TABLE_NAME, COLUMNS, USER_ID + " = ?", new String[]{Utils.USER_ID}, null, null, null);
            if (cursor.moveToLast()) {
                dictionary = new Dictionary();
                dictionary.setId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
                dictionary.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(TITLE)));
                dictionary.setContent(cursor.getString(cursor.getColumnIndexOrThrow(CONTENT)));
            }
            cursor.close();
        } catch(Exception e) {

        } finally {
            db.close();
        }
        return dictionary;
    }

    public boolean removeDictionary(int id) {
        SQLiteDatabase db = getWritableDatabase();
        String where = _ID + " = ?";
        String[] args = {String.valueOf(id)};
        int rows = db.delete(TABLE_NAME, where, args);
        db.close();
        return rows > 0;
    }

    public boolean insert(Dictionary item) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TITLE, item.getTitle());
        values.put(CONTENT, item.getContent());
        values.put(USER_ID, Utils.USER_ID);
        long rows = db.insert(TABLE_NAME, null, values);
        db.close();
        return rows > 0;
    }

}

package com.cirilo.cabrera.sergio.mydictionary;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.cirilo.cabrera.sergio.mydictionary.model.Dictionary;

public class DictionaryDetailsFragment extends Fragment {

    public static final String ITEM = "item";

    private TextView txvTitle;
    private TextView txvContent;
    private Dictionary dictionary;

    public static DictionaryDetailsFragment newInstance(Bundle bundle) {
        DictionaryDetailsFragment fragment = new DictionaryDetailsFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dictionary_details, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initComponents(view);
        initData();

    }

    private void initComponents(View root) {
        txvTitle = root.findViewById(R.id.txvDetailTitle);
        txvContent = root.findViewById(R.id.txvDetailContent);
    }

    private void initData() {
        Bundle arguments = getArguments();
        if (arguments != null) {
            dictionary = (Dictionary) arguments.getSerializable(ITEM);
            if(dictionary != null) {
                txvTitle.setText(dictionary.getTitle());
                txvContent.setText(dictionary.getContent());
            }
        }
    }
}
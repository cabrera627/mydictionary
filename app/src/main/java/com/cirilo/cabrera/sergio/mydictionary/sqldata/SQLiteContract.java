package com.cirilo.cabrera.sergio.mydictionary.sqldata;

import android.provider.BaseColumns;

public final class SQLiteContract {

    public static class DictionaryEntries implements BaseColumns {
        public static final String TABLE_NAME = "dictionary";
        public static final String TITLE = "title";
        public static final String CONTENT = "subtitle";
        public static final String USER_ID = "userid";

        public static String[] COLUMNS = { _ID, TITLE, CONTENT };

    }

    public static class UserEntries implements BaseColumns {
        public static final String TABLE_NAME = "user";
        public static final String ID = "id";
        public static final String NAME = "name";

        public static String[] COLUMNS = { ID, NAME };

    }

}

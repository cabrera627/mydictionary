package com.cirilo.cabrera.sergio.mydictionary;

import android.content.Intent;
import android.os.Bundle;

import com.cirilo.cabrera.sergio.mydictionary.list.DictionaryAdapter;
import com.cirilo.cabrera.sergio.mydictionary.list.DictionaryHolder;
import com.cirilo.cabrera.sergio.mydictionary.model.Dictionary;
import com.cirilo.cabrera.sergio.mydictionary.service.CounterService;
import com.cirilo.cabrera.sergio.mydictionary.sqldata.DBManager;
import com.cirilo.cabrera.sergio.mydictionary.util.NetworkRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.FragmentTransaction;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.format.DateUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static com.cirilo.cabrera.sergio.mydictionary.constants.LoginConstants.PREF_LAST_CONNECTION;
import static com.cirilo.cabrera.sergio.mydictionary.constants.LoginConstants.PREF_TIME_CONNECTED;
import static com.cirilo.cabrera.sergio.mydictionary.constants.LoginConstants.PREF_USER_ID;
import static com.cirilo.cabrera.sergio.mydictionary.constants.LoginConstants.PREF_USER_LOCATION;
import static com.cirilo.cabrera.sergio.mydictionary.constants.LoginConstants.PREF_USER_NAME;

public class MainActivity extends BaseActivity implements FormDictionaryFragment.Listeners {
    private FrameLayout formframe;
    private TextView txvName;
    private TextView txvID;
    private TextView txvLastConnection;
    private FloatingActionButton fabAdd;
    private RecyclerView recyclerView;
    private DictionaryAdapter adapter;
    private ImageView imvLogout;
    private TextView txvWeather;
    private TextView txvTimeConnected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        writePreferences(PREF_LAST_CONNECTION, System.currentTimeMillis());
        initComponents();
        initListeners();
        initData();
        initListData();
        System.out.println("Total: " + getFromPreferences(PREF_TIME_CONNECTED, 0L));
    }

    private void initComponents() {
        txvName = findViewById(R.id.txvName);
        txvID = findViewById(R.id.txvID);
        txvLastConnection = findViewById(R.id.txvLastConnection);
        fabAdd = findViewById(R.id.fabAdd);
        recyclerView = findViewById(R.id.dictionaryList);
        formframe = findViewById(R.id.formFrame);
        imvLogout = findViewById(R.id.imgLogout);
        txvWeather = findViewById(R.id.weather);
        txvTimeConnected = findViewById(R.id.txvTimeConnected);
    }

    private void initListeners() {
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FormDictionaryFragment fragment = new FormDictionaryFragment();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.formFrame, fragment);
                transaction.commit();
                recyclerView.setVisibility(View.GONE);
                formframe.setVisibility(View.VISIBLE);
                fabAdd.hide();
            }
        });

        imvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
    }

    private void initData() {
        String name = (String) getFromPreferences(PREF_USER_NAME, "");
        String id = (String) getFromPreferences(PREF_USER_ID, "");
        Long date = (Long) getFromPreferences(PREF_LAST_CONNECTION, 0L);

        txvName.setText(name);
        txvID.setText(id);
        txvLastConnection.setText(DateUtils.formatDateTime(this, date, DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME));

        NetworkRequest requestWeather = new NetworkRequest(this);
        requestWeather.execute((String) getFromPreferences(PREF_USER_LOCATION, "Mexico"));
    }

    private void initListData() {
        DBManager db = new DBManager(this);
        List<Dictionary> list = db.getDictionaryList();

        adapter = new DictionaryAdapter(list, new DictionaryHolder.OnItemClickListener() {
            @Override
            public void onItemClick(Dictionary item, int position) {
                Bundle bundle = new Bundle();
                bundle.putSerializable(DictionaryDetailsFragment.ITEM, item);
                DictionaryDetailsFragment fragment = DictionaryDetailsFragment.newInstance(bundle);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.formFrame, fragment);
                transaction.commit();
                recyclerView.setVisibility(View.GONE);
                formframe.setVisibility(View.VISIBLE);
                fabAdd.hide();
            }

            @Override
            public void onDeleteClick(Dictionary item, int position) {
                DBManager db = new DBManager(MainActivity.this);
                if(db.removeDictionary(item.getId())) {
                    adapter.remove(position);
                    adapter.notifyDataSetChanged();
                } else {
                    showAlert(R.string.main_error_on_delete);
                }

            }
        });

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                ((LinearLayoutManager) recyclerView.getLayoutManager()).getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        if(formframe.getVisibility() == View.VISIBLE) {
            fabAdd.show();
            formframe.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public void showAlert(@StringRes int message) {
        showAlert(this, message);
    }

    @Override
    public void dataHasChanged(Dictionary dictionary) {
        adapter.addItem(dictionary);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void performBack() {
        onBackPressed();
    }

    @Override
    public void onResponseSuccess(JSONObject data) {
        try {
            String temperature = data.getJSONObject("current").getString("temperature");
            String name = data.getJSONObject("location").getString("name");
            String region = data.getJSONObject("location").getString("region");
            txvWeather.setText(getResources().getString(R.string.main_weather, temperature, name, region));
        }catch(JSONException e) {
            showAlert(this, e.getMessage());
        }
    }

    @Override
    public void onPrepare() {
        super.onPrepare();
        findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
    }

    @Override
    public void onProgress(int percentage) {
        super.onProgress(percentage);
        txvWeather.setText(getResources().getString(R.string.main_percentage, percentage));
    }

    @Override
    public void onResponseFinish() {
        super.onResponseFinish();
        findViewById(R.id.progressBar).setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = new Intent(MainActivity.this, CounterService.class);
        startService(intent);

        Long timeConnected = (Long) getFromPreferences(PREF_TIME_CONNECTED, 0L);
        String data = String.format(Locale.getDefault(), "Tiempo activo: %02d Días %02d:%02d:%02d", TimeUnit.SECONDS.toDays(timeConnected), TimeUnit.SECONDS.toHours(timeConnected), TimeUnit.SECONDS.toMinutes(timeConnected), timeConnected % 60);
        txvTimeConnected.setText(data);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Intent intent = new Intent(MainActivity.this, CounterService.class);
        stopService(intent);
    }

}
package com.cirilo.cabrera.sergio.mydictionary.list;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cirilo.cabrera.sergio.mydictionary.R;
import com.cirilo.cabrera.sergio.mydictionary.model.Dictionary;

public class DictionaryHolder extends RecyclerView.ViewHolder {
    protected LinearLayout layout;
    protected TextView title;
    protected ImageView delete;
    protected Dictionary item;
    protected int position;
    public OnItemClickListener listener;

    public DictionaryHolder(@NonNull View itemView) {
        super(itemView);
        layout = itemView.findViewById(R.id.itemContent);
        title = itemView.findViewById(R.id.itemTitle);
        delete = itemView.findViewById(R.id.itemDelete);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(item, position);
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onDeleteClick(item, position);
            }
        });
    }


    public interface OnItemClickListener {
        void onItemClick(Dictionary item, int position);

        void onDeleteClick(Dictionary item, int position);
    }

}

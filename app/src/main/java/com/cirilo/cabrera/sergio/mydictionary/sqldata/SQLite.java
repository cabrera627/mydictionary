package com.cirilo.cabrera.sergio.mydictionary.sqldata;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import static android.provider.BaseColumns._ID;

public class SQLite extends SQLiteOpenHelper {
    private final static int VERSION = 1;
    private final static String DB_NAME = "dictionaryappdb";


    private final String CREATE_DICTIONARY_TABLE =
            "CREATE TABLE IF NOT EXISTS " + SQLiteContract.DictionaryEntries.TABLE_NAME + "(" +
                    _ID + " INTEGER PRIMARY KEY, " +
                    SQLiteContract.DictionaryEntries.TITLE + " TEXT, " +
                    SQLiteContract.DictionaryEntries.CONTENT + " TEXT, " +
                    SQLiteContract.DictionaryEntries.USER_ID + " TEXT " +
            ");";

    private final String CREATE_USER_TABLE =
            "CREATE TABLE IF NOT EXISTS " + SQLiteContract.UserEntries.TABLE_NAME + "(" +
                    _ID + " INTEGER PRIMARY KEY, " +
                    SQLiteContract.UserEntries.ID + " TEXT, " +
                    SQLiteContract.UserEntries.NAME + " TEXT " +
                    ");";

    protected SQLite(@Nullable Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_DICTIONARY_TABLE);
        db.execSQL(CREATE_USER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + SQLiteContract.DictionaryEntries.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + SQLiteContract.UserEntries.TABLE_NAME);
    }


}

package com.cirilo.cabrera.sergio.mydictionary.constants;

public class LoginConstants {
    public final static String PREF_IS_LOGIN = "prefIsLogin";
    public final static String PREF_USER_NAME = "prefUserName";
    public final static String PREF_USER_LOCATION = "prefUserLocation";
    public final static String PREF_USER_ID = "prefUserId";
    public final static String PREF_LAST_CONNECTION = "prefLastConnection";

    public final static String PREF_TIME_CONNECTED = "prefTimeConnected";
}

package com.cirilo.cabrera.sergio.mydictionary;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.cirilo.cabrera.sergio.mydictionary.util.NetworkRequest;

import org.json.JSONObject;

import static com.cirilo.cabrera.sergio.mydictionary.constants.LoginConstants.PREF_IS_LOGIN;

public class BaseActivity extends AppCompatActivity implements NetworkRequest.OnRequestFinishListener {
    private final static String NAME = "MyDictionary";
    SharedPreferences preferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = getSharedPreferences(NAME, Context.MODE_PRIVATE);
    }

    protected void showAlert(Activity activity, @StringRes int messageId) {
        showAlert(activity, getResources().getString(messageId));
    }

    protected void showAlert(Activity activity, String message) {
        AlertDialog alert = new AlertDialog.Builder(activity)
                .setTitle(R.string.app_name)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null)
                .create();
        alert.show();
    }

    protected Object getFromPreferences(String key, Object defaultValue) {
        if(defaultValue instanceof String)
            return preferences.getString(key, (String)defaultValue);
        if(defaultValue instanceof Long)
            return preferences.getLong(key, (Long) defaultValue);
        if(defaultValue instanceof Integer)
            return preferences.getInt(key, (Integer) defaultValue);
        if(defaultValue instanceof Boolean)
            return preferences.getBoolean(key, (Boolean) defaultValue);
        return null;
    }

    protected void writePreferences(String key, Object value) {
        SharedPreferences.Editor editor = preferences.edit();
        if(value instanceof String)
            editor.putString(key, (String)value);
        if(value instanceof Long)
            editor.putLong(key, (Long)value);
        if(value instanceof Integer)
            editor.putInt(key, (Integer)value);
        if(value instanceof Boolean)
            editor.putBoolean(key, (Boolean)value);
        editor.apply();
    }

    protected void logout() {
        preferences.edit().clear().apply();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    protected boolean isLogged() {
        return preferences.getBoolean(PREF_IS_LOGIN, false);
    }


    @Override
    public void onResponseSuccess(JSONObject data) {
    }

    @Override
    public void onResponseError(int code, String message) {
        showAlert(this, message);
    }

    @Override
    public void onResponseFinish() {
    }

    @Override
    public void onPrepare() {
    }

    @Override
    public void onProgress(int percentage) {
    }
}
